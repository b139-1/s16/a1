console.log("Hello World!");

let number = parseInt(prompt("Enter a number:"));

//ITEMS 3 - 7
console.log(`The number provided is ${number}`);
for(number; number > 0; number--) {
	if(number <= 50) {
		console.warn(`Value is currently at ${number}, terminating loop now. Thank you!`);
		break;
	}
	else if(number % 10 === 0) {
		console.log(`Number ${number} is being skipped...`);
		continue;
	}
	else if(number % 5 === 0) {
		console.log(`${number} is divisible by 5, next number...`);
	}
}

// ITEMS 8 - 12
let str = 'supercalifragilisticexpialidocious';
let consonants = [];

console.log(str);
for(let i = 0; i < str.length; i++) {
	if(str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u') {
		continue;
	} 
	else {
		consonants[i] = str[i];
	}
}
console.log(consonants.join(""));
